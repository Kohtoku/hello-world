import java.util.Scanner;

public class HelloWorld2 {
    public static void main(String[] args){
        Scanner userIn = new Scanner(System.in);
        System.out.print("Your input is: ");
        String yourInput = userIn.nextLine();
        userIn.close();
        System.out.println(yourInput);
    }
}
